Symulator cząstki na płaszczyźnie w polach elektrostatycznym i magnetycznym.

Pole elektrostatyczne może być centralne lub jedorodne.

Kierunek indukcji magnetycznej jest prostopadły do płaszczyzny. Zwrot dodatni to zwrot od ekranu w stronę użytkownika.

Program pomija:
 - efekty relatywistyczne
 - wpływ ładunku cząstki na pole elektrostatyczne
 - wpływ ruchu naładowanej cząstki na pole magnetyczne
 - utratę energii przez cząstkę wskutek emisji fal elektromagnetycznych


Prędkość jest podawana w px/50ms = 20px/s.